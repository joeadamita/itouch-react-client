import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import './index.css';
//import App from './App';
import './App.css';
import * as serviceWorker from './serviceWorker';

import Login from './components/Login';
import Dashboard from './components/Dashboard';

ReactDOM.render(
<BrowserRouter>
    <Route exact path="/" component={Login} />
    <Route exact path="/dashboard" component={Dashboard}/>
</BrowserRouter>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
