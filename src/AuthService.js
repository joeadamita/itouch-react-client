import axios from 'axios';
import Cookies from 'js-cookie';
import decode from 'jwt-decode';

export default class AuthService {

    constructor() {
        this.login = this.login.bind(this);
    }

    login(username, password) {
        var user = { username, password };
        return axios.post('http://167.99.7.234/api/v1/user/login', user).then(res => {
            if (res.status === 200) {
                return Promise.resolve(res);
            }
        }).catch(err => {
            return Promise.reject(err);
        });
    }



    isAdmin() {
        if (this.loggedIn()) {
            const token = this.getToken();
            const decoded = decode(token);
            return (decoded.user.role === 'admin');
        } else {
            return false;
        }
    }

    loggedIn() {
        const token = this.getToken();
        return !!token && !this.isTokenExpired(token);
    }

    logout() {
        Cookies.remove('token');
    }

    isTokenExpired(token) {
        try {
            const decoded = decode(token);
            if (decoded.exp < Date.now() / 1000) {
                return true;
            } else {
                return false;
            }
        }
        catch (err) {
            return false;
        }
    }

    getUser() {
        // return decode(this.getToken());
        const token = this.getToken();
        const decoded = decode(token);
        return (decoded.user);
    }

    setToken(token) {
        Cookies.set('token', token);
    }

    getToken() {
        return Cookies.get('token');
    }
}