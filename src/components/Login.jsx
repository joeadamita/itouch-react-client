import React, { Component } from 'react';
import '../styles/Login.css';

import { Button, Icon, Input, Form, message } from 'antd';

import logo from '../logo.png';

import AuthService from '../AuthService';

class Login extends Component {
    constructor() {
        super();

        this.state = {
            username: '',
            password: '',
            error: false
        };

        this.Auth = new AuthService();
        
        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onAlertClose = this.onAlertClose.bind(this);
    }

    componentWillMount() {
        if (this.Auth.loggedIn()) {
            this.props.history.replace('/dashboard');
            console.log('Your already logged in silly!!');
        }
    }

    onAlertClose(e) {
        this.setState({ error: false });
    };

    onSubmit(e) {
        e.preventDefault();
        this.Auth.login(this.state.username, this.state.password).then(res => {
            if (res.status === 200) {
                //console.log('Login Succesful!');
                this.props.history.replace('/dashboard');
            }
            
        }).catch(err => {
            this.setState({ error: true });
            message.error('Error! Incorrect username or password!', 2);
        });
    }

    onChange = (e) => {
        this.setState({someVal: e.target.value})
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        return (
            <div className="login-form">
                <div className="login-logo">
                    <img src={logo} height="45" alt="iTouchOrlando Logo"/>
                </div>
                <Form onSubmit={ this.onSubmit }>
                    <Form.Item>
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)'}} />}
                            placeholder="Username"
                            onChange={evt => this.setState({ username: evt.target.value })}
                            value={this.state.username}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Input 
                            prefix={<Icon type="lock" 
                            style={{ color: 'rgba(0,0,0,.25)'}} />} 
                            placeholder="Password" 
                            type="password"
                            onChange={evt => this.setState({ password: evt.target.value })}
                            value={this.state.password}
                        />
                    </Form.Item>
                    <Form.Item>
                        <div className="login-btn">
                            <Button type="primary" htmlType="submit">Sign in</Button>
                        </div>
                    </Form.Item>
                </Form>
            </div>
        );
    }
}

export default Login;