import React from 'react';
import { Modal, Form, Input, Switch, Row, Col  } from 'antd';

const EditKiosk = Form.create({ name: 'form_in_modal' })(
    class extends React.Component {
        render() {
            const {
                visible, onCancel, onCreate, form
            } = this.props;
            const { getFieldDecorator } = form;
            return (
            <Modal
                visible={visible}
                title="Edit Kiosk"
                okText="Edit"
                onCancel={onCancel}
                onOk={onCreate}
                centered
                width={820}
            >
                <Form>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Form.Item label="Kiosk Name">
                                {getFieldDecorator('kiosk_name', { initialValue: this.props.kiosk.name })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Status">
                                {getFieldDecorator('status', { initialValue: this.props.kiosk.status })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Placement">
                                {getFieldDecorator('placement', { initialValue: this.props.kiosk.placement })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Notes">
                                {getFieldDecorator('notes', { initialValue: this.props.kiosk.note })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Badge Image URL">
                                {getFieldDecorator('badge_url', { initialValue: this.props.kiosk.badgeImage })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Show Expedia?">
                                {getFieldDecorator('switch', { valuePropName: 'checked' })(
                                    <Switch />
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="Contact Title">
                                {getFieldDecorator('title', { initialValue: this.props.kiosk.contact_title })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Contact First Name">
                                {getFieldDecorator('first_name', { initialValue: this.props.kiosk.contact_firstName })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Contact Last Name">
                                {getFieldDecorator('last_name', { initialValue: this.props.kiosk.contact_lastName })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Contact Email">
                                {getFieldDecorator('email', { initialValue: this.props.kiosk.contact_email })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Contact Direct Phone">
                                {getFieldDecorator('direct_phone', { initialValue: this.props.kiosk.contact_directPhone })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Contact Cell Phone">
                                {getFieldDecorator('cell_phone', { initialValue: this.props.kiosk.contact_cellphone })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Zipcode">
                                {getFieldDecorator('zipcode', { initialValue: this.props.kiosk.zipcode })(<Input type="textarea" />)}
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="Address Street">
                                {getFieldDecorator('street1', { initialValue: this.props.kiosk.address_street1 })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Address Street 2">
                                {getFieldDecorator('street2', { initialValue: this.props.kiosk.address_street2 })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="City">
                                {getFieldDecorator('city', { initialValue: this.props.kiosk.address_city })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="State">
                                {getFieldDecorator('state', { initialValue: this.props.kiosk.address_state })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Address Zipcode">
                                {getFieldDecorator('address_zipcode', { initialValue: this.props.kiosk.address_zip })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Location">
                                {getFieldDecorator('location', { initialValue: this.props.kiosk.location })(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Teamviewer ID">
                                {getFieldDecorator('tvid', { initialValue: this.props.kiosk.tvid })(<Input type="textarea" />)}
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
            );
        }
    }
);

export default EditKiosk;