import React from 'react';
import { Modal, Form, Input, Switch, Row, Col  } from 'antd';

const NewKiosk = Form.create({ name: 'form_in_modal' })(
    class extends React.Component {
        render() {
            const {
                visible, onCancel, onCreate, form,
            } = this.props;
            const { getFieldDecorator } = form;
            return (
            <Modal
                visible={visible}
                title="Create a new Kiosk"
                okText="Create"
                onCancel={onCancel}
                onOk={onCreate}
                centered
                width={820}
            >
                <Form>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Form.Item label="Kiosk Name">
                                {getFieldDecorator('kiosk_name')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Status">
                                {getFieldDecorator('status')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Placement">
                                {getFieldDecorator('placement')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Notes">
                                {getFieldDecorator('notes')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Badge Image URL">
                                {getFieldDecorator('badge_url')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Show Expedia?">
                                {getFieldDecorator('switch', { valuePropName: 'checked' })(
                                    <Switch />
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="Contact Title">
                                {getFieldDecorator('title')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Contact First Name">
                                {getFieldDecorator('first_name')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Contact Last Name">
                                {getFieldDecorator('last_name')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Contact Email">
                                {getFieldDecorator('email')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Contact Direct Phone">
                                {getFieldDecorator('direct_phone')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Contact Cell Phone">
                                {getFieldDecorator('cell_phone')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Zipcode">
                                {getFieldDecorator('zipcode')(<Input type="textarea" />)}
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="Address Street">
                                {getFieldDecorator('street1')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Address Street 2">
                                {getFieldDecorator('street2')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="City">
                                {getFieldDecorator('city')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="State">
                                {getFieldDecorator('state')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Address Zipcode">
                                {getFieldDecorator('address_zipcode')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Location">
                                {getFieldDecorator('location')(<Input type="textarea" />)}
                            </Form.Item>
                            <Form.Item label="Teamviewer ID">
                                {getFieldDecorator('tvid')(<Input type="textarea" />)}
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
            );
        }
    }
);

export default NewKiosk;