import React from 'react';
import axios from 'axios';

import { Table, Card, Statistic, Row, Col, Spin, Button, Select, Menu, DatePicker, AutoComplete, Input, Icon } from 'antd';

const { RangePicker } = DatePicker;
const { Option } = Select;

class Analytics extends React.Component {

    constructor() {
        super();

        this.state = {
            listingNames: [],
            autoData: ['Adam', 'Bryce', 'Connor', 'Dom', 'Eric', 'Frank', 'Gary', 'Harry', 'Ivan', 'Joe'],
            touchData: [],
            logUnit: 'thisYear',
            loading: true
        };

        this.refreshStats = this.refreshStats.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.getListingNames = this.getListingNames.bind(this);
    }
    

    handleChange(value) {
        console.log(`selected ${value}`);
        this.setState({ logUnit: value });
    }

    onSelect(value) {
        console.log('onSelect', value);
    }

    handleSearch(value) {
        console.log('handleSearch', value);
    }

    getListingNames() {
        var retArray = [];
        axios.get('http://167.99.7.234/api/v1/listing/').then(res => {
            if (res.data != null) {
                res.data.forEach(listing => {
                    retArray.push(listing.name);
                });
                this.setState({ listingNames: retArray });
            }
        });
    }
    
    refreshStats() {
        this.setState({ loading: true });

        switch(this.state.logUnit) {
            case 'thisYear':
                console.log('year shit');
                    axios.get('http://167.99.7.234/api/v1/log/year/1').then(res => {
                        if (res.data != null) {
                            this.setState({ loading: false });
                            this.setState({ touchData: res.data });
                        }
                    });
                break;

                case 'thisMonth':
                    console.log('month shit');
                        axios.get('http://167.99.7.234/api/v1/log/month/1').then(res => {
                            if (res.data != null) {
                                this.setState({ loading: false });
                                this.setState({ touchData: res.data });
                            }
                        });
                    break;

                        

            default:
                console.log('123');
                break;
        }
    }

    componentWillMount() {
        console.log('analytics will mount!');
        this.getListingNames();
        this.refreshStats();
    }
    
    render() {

        const menu = (
            <Menu>
                <Menu.Item>
                    1st menu item
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">
                        2nd menu item
                    </a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank" rel="noopener noreferrer" href="http://www.tmall.com/">
                        3rd menu item
                    </a>
                </Menu.Item>
            </Menu>
        );

        const tableColumns = [
            {
                title: 'Time',
                dataIndex: 'timestamp'
            },
            {
                title: 'Kiosk Name',
                dataIndex: 'kioskName',
            },
            {
                title: 'Listing ID',
                dataIndex: 'listingId'
            },
            {
                title: 'Action ID',
                dataIndex: 'action'
            },
            {
                title: 'Details',
                dataIndex: 'details'
            }
        ];

        let cards = null;

        if (this.state.loading) {
            cards = <Row gutter={16}>
            <Col span={4}>
                <Card>
                    <Statistic
                        title="Kisosks"
                        value=' '
                        valueStyle={{ color: '#3f8600' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Touches"
                        value=' '
                        valueStyle={{ color: '#3f8600' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Emails"
                        value=' '
                        valueStyle={{ color: '#3f8600' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="SMS"
                        value=' '
                        valueStyle={{ color: '#3f8600' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Coupons"
                        value=' '
                        valueStyle={{ color: '#3f8600' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Tickets"
                        value=' '
                        valueStyle={{ color: '#3f8600' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
        </Row>
        } else {
            cards = <Row gutter={16}>
            <Col span={4}>
                <Card>
                    <Statistic
                        title="Kisosks"
                        value={this.state.touchData.kiosk_count}
                        valueStyle={{ color: '#3f8600' }}
                        //prefix={<Icon type="desktop" />}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Touches"
                        value={this.state.touchData.touch_count}
                        valueStyle={{ color: '#3f8600' }}
                        //prefix={<Icon type="arrow-down" />}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Emails"
                        value={this.state.touchData.email_count}
                        valueStyle={{ color: '#3f8600' }}
                        //prefix={<Icon type="arrow-up" />}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="SMS"
                        value={this.state.touchData.sms_count}
                        valueStyle={{ color: '#3f8600' }}
                        // prefix={<Icon type="arrow-up" />}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Coupons"
                        value={this.state.touchData.coupon_count}
                        valueStyle={{ color: '#3f8600' }}
                        // prefix={<Icon type="arrow-up" />}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Tickets"
                        value={this.state.touchData.ticket_count}
                        valueStyle={{ color: '#3f8600' }}
                        //prefix={<Icon type="arrow-down" />}
                    />
                </Card>
            </Col>
        </Row>
        }

        return (
            <div>
                {cards}
                <br/>
                <Card>
                    <h2>Analytic Stats</h2>
                    <Select size="large" defaultValue="thisYear" style={{ width: 250 }} onChange={this.handleChange}>
                        <Option value="thisYear">This Year</Option>
                        <Option value="thisMonth">This Month</Option>
                        <Option value="today ">Today</Option>
                    </Select>
                    {'                         '}
                    <RangePicker size="large" />
                    {'                         '}
                    <Button size="large" type="primary" onClick={this.refreshStats}>Refresh</Button>
                    <br/>
                    <br/>
                    <AutoComplete
                        size="large"
                        dataSource={this.state.listingNames}
                        style={{ width: 250 }}
                        onSelect={this.onSelect}
                        onSearch={this.handleSearch}
                        placeholder="Listing Name Search"
                        filterOption={(inputValue, option) =>
                            option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                        }
                    >
                        <Input
                            suffix={
                            <Button
                                className="search-btn"
                                style={{ marginRight: -12 }}
                                size="large"
                                type="primary"
                            >
                                <Icon type="search" />
                            </Button>
                            }
                        />
                    </AutoComplete>
                </Card>
                <br/>
                <Card>
                    <h2>Analytic Data</h2>
                    <Table columns={tableColumns}/>
                </Card>
            </div>

        );
    }
}

export default Analytics;