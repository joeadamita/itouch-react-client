import React from 'react';
import axios from 'axios';
import { Statistic, Card, Row, Col, Icon, Spin } from 'antd';
import { ChartCard, MiniArea, WaterWave  } from 'ant-design-pro/lib/Charts';
import NumberInfo from 'ant-design-pro/lib/NumberInfo';

import moment from 'moment';
import Axios from 'axios';


class Home extends React.Component {

    constructor() {
        super();

        this.state = {
            touchData: [],
            loading: true
        };
    }

    componentWillMount() {
        const visitData = [];
        for (let i = 0; i < 20; i += 1) {
            visitData.push({
                x: moment(new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * i)).format('YYYY-MM-DD'),
                y: Math.floor(Math.random() * 100) + 10,
            });
        }

        axios.get('http://167.99.7.234/api/v1/log/year/1').then(res => {
            if (res.data != null) {
                this.setState({ loading: false });
                this.setState({ touchData: res.data });
            }
        });

        //this.setState({ touchData: visitData });
    }



    render() {

        let cards = null;

        if (this.state.loading) {
            cards = <Row gutter={16}>
            <Col span={4}>
                <Card>
                    <Statistic
                        title="Kisosks"
                        value=' '
                        valueStyle={{ color: '#478d47' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Touches"
                        value=' '
                        valueStyle={{ color: '#708090' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Emails"
                        value=' '
                        valueStyle={{ color: '#474479' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="SMS"
                        value=' '
                        valueStyle={{ color: '#474479' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Coupons"
                        value=' '
                        valueStyle={{ color: '#f06709' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Tickets"
                        value=' '
                        valueStyle={{ color: '#904597' }}
                        prefix={<Spin/>}
                    />
                </Card>
            </Col>
        </Row>
        } else {
            cards = <Row gutter={16}>
            <Col span={4}>
                <Card>
                    <Statistic
                        title="Kisosks"
                        value={this.state.touchData.kiosk_count}
                        valueStyle={{ color: '#478d47' }}
                        //prefix={<Icon type="desktop" />}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Touches"
                        value={this.state.touchData.touch_count}
                        valueStyle={{ color: '#708090' }}
                        //prefix={<Icon type="arrow-down" />}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Emails"
                        value={this.state.touchData.email_count}
                        valueStyle={{ color: '#474479' }}
                        //prefix={<Icon type="arrow-up" />}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="SMS"
                        value={this.state.touchData.sms_count}
                        valueStyle={{ color: '#474479' }}
                        // prefix={<Icon type="arrow-up" />}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Coupons"
                        value={this.state.touchData.coupon_count}
                        valueStyle={{ color: '#f06709' }}
                        // prefix={<Icon type="arrow-up" />}
                    />
                </Card>
            </Col>
            <Col span={4}>
                <Card>
                <Statistic
                        title="Tickets"
                        value={this.state.touchData.ticket_count}
                        valueStyle={{ color: '#904597' }}
                        //prefix={<Icon type="arrow-down" />}
                    />
                </Card>
            </Col>
        </Row>
        }

        return (
            <div>
                {cards}
                <br/>
                {/* <Row gutter={16}>
                    <Col span={12}>
                        <ChartCard 
                            title="Touches"
                            total="8336" 
                            contentHeight={134}
                        >
                        </ChartCard>
                    </Col>
                    <Col span={12}>
                        <ChartCard 
                            title="Emails"
                            total="146" 
                            contentHeight={134}
                        >
                            
                        </ChartCard>
                    </Col>
                </Row> */}
            </div>
        );
    }
}

export default Home;