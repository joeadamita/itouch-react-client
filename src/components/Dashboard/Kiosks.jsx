import React from 'react';
import axios from 'axios';
import moment from 'moment';

import { Table, Card, Button, Divider, Icon, Tag, Modal, Tooltip, Tabs } from 'antd';

import NewKioskModal from './Forms/NewKiosk'
import EditKioskModal from './Forms/EditKiosk'

class Kiosks extends React.Component {

    constructor() {
        super();

        this.state = {
            kiosks: [],
            selectedKiosk: null,
            visible: false,
            editVisible: false,
            upKiosks: [],
            downKiosks: [],
            deadKiosks: []
        };

        this.refreshKiosks = this.refreshKiosks.bind(this);
        this.callback = this.callback.bind(this);
    }

    componentWillMount() {
        // axios.get('http://167.99.7.234/api/v1/kiosk/').then((res) => {
        //     this.setState({kiosks: res.data});
        // }).catch((err) => {
        //     alert('Error - ' + err.message);
        // });
        this.refreshKiosks();
    }

    success(title, message) {
        Modal.success({
            title: title,
            content: message,
        });
    }

    handleCancel = () => {
        this.setState({ visible: false });
        this.setState({ editVisible: false });
    }

    showEditModal = () => {
        this.setState({ editVisible: true });
    }

    showModal = () => {
        this.setState({ visible: true });
    }

    editKiosk(kiosk) {
        if (this.state.selectedKiosk != null) {
            console.log(this.state.selectedKiosk);
            this.setState({ editVisible: true });
        }
    }

    deleteKiosk(kiosk) {
        alert(`Delete Kiosk: ${kiosk.name}`);
    }

    handleCreate() {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);
            form.resetFields();
            this.setState({ visible: false });
        });
    }

    refreshKiosks() {
        console.log('STARTING REFRESH!');

        var kiosks = [];
        var upKiosks = [];
        var downKiosks = [];
        var deadKiosks = [];
        axios.get('http://167.99.7.234/api/v1/kiosk/').then((res) => {
            kiosks = res.data;

            kiosks.forEach(kiosk => {
                if (kiosk.status === 'Inactive') {
                    deadKiosks.push(kiosk);
                } else {
                    var timeDiff = this.getTimeDifference(kiosk.lastPing);

                    if (timeDiff < 15) {
                        upKiosks.push(kiosk);
                    } else {
                        downKiosks.push(kiosk);
                    }
                }
            });

            //console.log(deadKiosks); GOOD
            //console.log(downKiosks);
            //console.log(upKiosks);
            this.setState({ upKiosks: upKiosks });
            this.setState({ deadKiosks: deadKiosks });
            this.setState({ downKiosks: downKiosks });
        }).catch((err) => {
            alert('Error - ' + err.message);
        });


        console.log('ENDING REFRESH!');
    }

    handleEditCreate() {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            const currentKiosk = {
                name: values.kiosk_name,
                status: values.status,
                placement: values.placement,
                note: values.notes,
                badgeImage: values.badge_url,
                zipcode: values.zipcode,
                contact_title: values.title,
                contact_firstName: values.first_name,
                contact_lastName: values.last_name,
                contact_email: values.email,
                contact_directPhone: values.direct_phone,
                contact_cellphone: values.cell_phone,
                address_street1: values.street1,
                address_street2: values.street2,
                address_city: values.city,
                address_state: values.state,
                address_zip: values.address_zipcode,
                showExpedia: values.switch || false,
                location: values.location,
                listings: values.listings,
                tvid: values.tvid
            }

            //console.log('Received values of form: ', values);
            form.resetFields();
            this.setState({ editVisible: false }, ()=>{
                console.log(currentKiosk);
                this.success('Success', 'Kiosk successfully updated!');
            });
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    getTimeDifference(item) {
        var currentTime = Date.now();
        var diff = currentTime - item;

        diff /= 1000;

        var seconds = Math.round(diff);
        var minutes = Math.round(seconds / 60);
        return(minutes); 
    }

    isActive(kiosk) {
        if (kiosk.status === 'Inactive')
            return <Tag>DEAD/OFFLINE</Tag>;
        if (this.getTimeDifference(kiosk.lastPing) < 15)
            return <Tag color="green">Online</Tag>;
        if (this.getTimeDifference(kiosk.lastPing) > 15)
            return (
                <Tooltip title={this.getTimeDifference(kiosk.lastPing) + ' Minutes ago'}>
                    <Tag color="red">Offline</Tag>
                </Tooltip>
            );
    }

    callback(key) {
        console.log(key);
    }

    render() {

        const { Column } = Table;

        const TabPane = Tabs.TabPane;

        let editKiosk = null;

        if (this.state.selectedKiosk) {
            editKiosk = <EditKioskModal
                wrappedComponentRef={this.saveFormRef}
                visible={this.state.editVisible}
                onCancel={this.handleCancel}
                kiosk={this.state.selectedKiosk}
                onCreate={this.handleEditCreate.bind(this)}
            />
        }

        return (
            <div>
                <Card>
                    <h1>Kiosks</h1>
                    <Tabs defaultActiveKey="1" onChange={this.callback} type="card">
                        <TabPane tab={<span><Icon theme="twoTone" twoToneColor="#52c41a" type="smile" />{`Online Kiosks (${this.state.upKiosks.length})`}</span>} key="1">
                            <Table 
                                dataSource={this.state.upKiosks}
                                pagination={{ size: 'small', defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['10', '20', '30', 'All']}}
                            >
                                <Column
                                    title="Kiosk Name"
                                    dataIndex="name"
                                    key="name"
                                />
                                <Column
                                    title="Location"
                                    dataIndex="location"
                                    key="location"
                                />
                                <Column
                                    title="Status"
                                    dataIndex="status"
                                    key="status"
                                />
                                <Column
                                    title="Ping"
                                    dataIndex="lastPing"
                                    key="lastPing"
                                    sorter= {(a, b) => a.lastPing - b.lastPing}
                                    render={(text, record) => (
                                        <span>
                                            {this.isActive(record)}
                                        </span>
                                    )}
                                />
                                <Column
                                    title="Version"
                                    dataIndex="version"
                                    key="version"
                                />
                                <Column
                                    title="Created"
                                    dataIndex="createdAt"
                                    key="createdAt"
                                    render={(text, record) => (
                                        <span>
                                            {moment(record.createdAt).format('MMMM Do, h:mm a')}
                                        </span>
                                    )}
                                />
                                <Column
                                    title="Last Updated"
                                    dataIndex="updatedAt"
                                    key="updatedAt"
                                    render={(text, record) => (
                                        <span>
                                            {moment(record.lastUpdate).format('MMMM Do, h:mm a')}
                                        </span>
                                    )}
                                />
                                <Column
                                    title="Actions "
                                    dataIndex="actions"
                                    key="actions"
                                    render={(text, record) => (
                                        <span>
                                            <Button onClick={()=>{
                                                this.setState({ selectedKiosk: record }, () => {
                                                    this.editKiosk(record)
                                                })
                                            }} type="danger" shape="circle"><Icon type="edit"/></Button>
                                            <Divider type="vertical" />
                                            <Button onClick={()=>this.deleteKiosk(record)} shape="circle"><Icon type="delete"/></Button>
                                        </span>
                                    )}
                                />
                            </Table>
                        </TabPane>
                        <TabPane tab={<span><Icon theme="twoTone" twoToneColor="#eb2f96" type="frown" />{`Down Kiosks (${this.state.downKiosks.length})`}</span>} key="2">
                            <Table 
                                dataSource={this.state.downKiosks}
                                pagination={{ size: 'small', defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['10', '20', '30', 'All']}}
                            >
                                <Column
                                    title="Kiosk Name"
                                    dataIndex="name"
                                    key="name"
                                />
                                <Column
                                    title="Location"
                                    dataIndex="location"
                                    key="location"
                                />
                                <Column
                                    title="Status"
                                    dataIndex="status"
                                    key="status"
                                />
                                <Column
                                    title="Ping"
                                    dataIndex="lastPing"
                                    key="lastPing"
                                    sorter= {(a, b) => a.lastPing - b.lastPing}
                                    render={(text, record) => (
                                        <span>
                                            {this.isActive(record)}
                                        </span>
                                    )}
                                />
                                <Column
                                    title="Version"
                                    dataIndex="version"
                                    key="version"
                                />
                                <Column
                                    title="Created"
                                    dataIndex="createdAt"
                                    key="createdAt"
                                    render={(text, record) => (
                                        <span>
                                            {moment(record.createdAt).format('MMMM Do, h:mm a')}
                                        </span>
                                    )}
                                />
                                <Column
                                    title="Last Updated"
                                    dataIndex="updatedAt"
                                    key="updatedAt"
                                    render={(text, record) => (
                                        <span>
                                            {moment(record.lastUpdate).format('MMMM Do, h:mm a')}
                                        </span>
                                    )}
                                />
                                <Column
                                    title="Actions "
                                    dataIndex="actions"
                                    key="actions"
                                    render={(text, record) => (
                                        <span>
                                            <Button onClick={()=>{
                                                this.setState({ selectedKiosk: record }, () => {
                                                    this.editKiosk(record)
                                                })
                                            }} type="danger" shape="circle"><Icon type="edit"/></Button>
                                            <Divider type="vertical" />
                                            <Button onClick={()=>this.deleteKiosk(record)} shape="circle"><Icon type="delete"/></Button>
                                        </span>
                                    )}
                                />
                            </Table>
                        </TabPane>
                        <TabPane tab={<span><Icon type="api" />{`Dead Kiosks (${this.state.deadKiosks.length})`}</span>} key="3">
                            <Table 
                                dataSource={this.state.deadKiosks}
                                pagination={{ size: 'small', defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['10', '20', '30', 'All']}}
                            >
                                <Column
                                    title="Kiosk Name"
                                    dataIndex="name"
                                    key="name"
                                />
                                <Column
                                    title="Location"
                                    dataIndex="location"
                                    key="location"
                                />
                                <Column
                                    title="Status"
                                    dataIndex="status"
                                    key="status"
                                />
                                <Column
                                    title="Ping"
                                    dataIndex="lastPing"
                                    key="lastPing"
                                    sorter= {(a, b) => a.lastPing - b.lastPing}
                                    render={(text, record) => (
                                        <span>
                                            {this.isActive(record)}
                                        </span>
                                    )}
                                />
                                <Column
                                    title="Version"
                                    dataIndex="version"
                                    key="version"
                                />
                                <Column
                                    title="Created"
                                    dataIndex="createdAt"
                                    key="createdAt"
                                    render={(text, record) => (
                                        <span>
                                            {moment(record.createdAt).format('MMMM Do, h:mm a')}
                                        </span>
                                    )}
                                />
                                <Column
                                    title="Last  Updated"
                                    dataIndex="updatedAt"
                                    key="updatedAt"
                                    render={(text, record) => (
                                        <span>
                                            {moment(record.lastUpdate).format('MMMM Do, h:mm a')}
                                        </span>
                                    )}
                                />
                                <Column
                                    title="Actions "
                                    dataIndex="actions"
                                    key="actions"
                                    render={(text, record) => (
                                        <span>
                                            <Button onClick={()=>{
                                                this.setState({ selectedKiosk: record }, () => {
                                                    this.editKiosk(record)
                                                })
                                            }} type="danger" shape="circle"><Icon type="edit"/></Button>
                                            <Divider type="vertical" />
                                            <Button onClick={()=>this.deleteKiosk(record)} shape="circle"><Icon type="delete"/></Button>
                                        </span>
                                    )}
                                />
                            </Table>
                        </TabPane>
                    </Tabs>
                    <Button style={{
                            position: 'absolute',
                            right: '140px',
                            top: '15px'
                        }} onClick={this.refreshKiosks}>Refresh</Button>
                    <Button style={{
                            position: 'absolute',
                            right: '30px',
                            top: '15px'
                        }} onClick={this.refreshKiosks}>New Kiosk</Button>

                    <NewKioskModal
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        onCreate={this.handleCreate.bind(this)}
                    />

                    {editKiosk}
                </Card>
            </div>
        );
    }
}

// Kiosk Name
// Location
// Status
// Ping
// Version
// Created
// Last Updated
// Actions

export default Kiosks;