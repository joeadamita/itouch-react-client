import React from 'react';
import '../styles/Dashboard.css';

import AuthService from '../AuthService';
import withAuth from './withAuth';

import { Layout, Menu, Icon, Button } from 'antd';

import logo from '../logo.png';
import logoSmall from '../logo_small.png';

import Home from './Dashboard/Home';
import Kiosks from './Dashboard/Kiosks';
import Listings from './Dashboard/Listings';
import Analytics from './Dashboard/Analytics';
import Users from './Dashboard/Users';
import Changelog from './Dashboard/Changelog';
import Settings from './Dashboard/Settings';

class Dashboard extends React.Component {

    constructor() {
        super();

        this.state = {
            activeMenu: 1,
            collapsed: false,
        };

        this.Auth = new AuthService();
        
        this.handleLogout = this.handleLogout.bind(this);
        this.handleMenuClick = this.handleMenuClick.bind(this);
    }

    handleLogout(){
        this.Auth.logout();
        this.props.history.replace('/');
    }

    onCollapse = (collapsed) => {
        console.log(collapsed);
        this.setState({ collapsed });
    }

    handleMenuClick(e) {
        switch (e.key) {
            default:
                console.log('default');
                break;
            case '1':
                console.log('1');
                this.setState({ activeMenu: 1 });
                break;
            case '2':
                console.log('2');
                this.setState({ activeMenu: 2 });
                break;
            case '3':
                console.log('3');
                this.setState({ activeMenu: 3 });
                break;
            case '4':
                console.log('4');
                this.setState({ activeMenu: 4 });
                break;
            case '5':
                console.log('5');
                this.setState({ activeMenu: 5 });
                break;
            case '6':
                console.log('6');
                this.setState({ activeMenu: 6 });
                break;
            case '7':
                console.log('7');
                this.setState({ activeMenu: 7 });
                break;
        }
    }

    render() {

        const {
            Header, Content, Footer, Sider,
        } = Layout;

        let adminMenu = null;

        if (this.Auth.isAdmin()) {
            adminMenu = <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                            <Menu.Item onClick={this.handleMenuClick} key="1"><Icon type="home" /><span>Home</span></Menu.Item>
                            <Menu.Item onClick={this.handleMenuClick} key="2"><Icon type="tablet" /><span>Kiosks</span></Menu.Item>
                            <Menu.Item onClick={this.handleMenuClick} key="3"><Icon type="appstore" /><span>Listings</span></Menu.Item>
                            <Menu.Item onClick={this.handleMenuClick} key="4"><Icon type="bar-chart" /><span>Analytics</span></Menu.Item>
                            <Menu.Item onClick={this.handleMenuClick} key="5"><Icon type="user" /><span>Users</span></Menu.Item>
                            <Menu.Item onClick={this.handleMenuClick} key="6"><Icon type="profile" /><span>Changelog</span></Menu.Item>
                            <Menu.Item onClick={this.handleMenuClick} key="7"><Icon type="tool" /><span>Settings</span></Menu.Item>
                        </Menu>
        } else {
            adminMenu = <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                            <Menu.Item onClick={this.handleMenuClick} key="1"><Icon type="home" /><span>Home</span></Menu.Item>
                            <Menu.Item onClick={this.handleMenuClick} key="4"><Icon type="bar-chart" /><span>Analytics</span></Menu.Item>
                            <Menu.Item onClick={this.handleMenuClick} key="6"><Icon type="profile" /><span>Changelog</span></Menu.Item>
                            <Menu.Item onClick={this.handleMenuClick} key="7"><Icon type="tool" /><span>Settings</span></Menu.Item>
                        </Menu>
        }

        let mainContent = null;

        switch (this.state.activeMenu) {
            default:
                mainContent = <Home/>
                break;
            case 1:
                mainContent = <Home/>
                break;
            case 2:
                mainContent = <Kiosks/>
                break;
            case 3:
                mainContent = <Listings/>
                break;
            case 4:
                mainContent = <Analytics/>
                break;
            case 5:
                mainContent = <Users/>
                break;
            case 6:
                mainContent = <Changelog/>
                break;
            case 7:
                mainContent = <Settings/>
                break;
        }

        let logoComp = null;

        if (this.state.collapsed) {
            logoComp = <div className="logo"><img src={logoSmall} height="45" alt="iTouchOrlando Logo"/></div>
        } else {
            logoComp = <div className="logo"><img src={logo} height="45" alt="iTouchOrlando Logo"/></div>
        }

        let userInfo = null;

        if (this.Auth.loggedIn) {
            userInfo = <h1 style={{
                position: 'absolute',
                // paddingLeft: '20px',
                fontSize: '25px',
                right: '125px'
            }}>Welcome {this.Auth.getUser().username}</h1>
        }

        return (
            <Layout style={{ minHeight: '100vh' }}>
                <Sider
                    collapsible
                    collapsed={this.state.collapsed}
                    onCollapse={this.onCollapse}
                >
                    {logoComp}
                    {adminMenu}}
                </Sider>
                <Layout>
                    <Header style={{ background: '#fff', padding: 0 }} >
                        {userInfo}
                        <Button style={{
                            position: 'absolute',
                            right: '30px',
                            top: '15px'
                        }} type="primary" onClick={this.handleLogout}>Logout</Button>
                    </Header>
                    <Content style={{ margin: '0 16px', background: '#e8edf3' }}>
                        <div style={{ padding: 24, background: '#e8edf3', minHeight: 600 }}>
                            {mainContent}
                        </div>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}> ©2019 iTouchOrlando</Footer>
                    </Layout>
                </Layout>
        );
    }
}

// Dashboard/Home
// Kisosk
// Listings
// Analytics
// Changelog
// Setting

export default withAuth(Dashboard);