import React, { Component } from 'react';
import AuthService from '../AuthService';

export default function withAdmin(AuthComponent) {
    const Auth = new AuthService();

    return class AuthWrapped extends Component {
        constructor() {
            super();
            this.state = {
                user: null
            }
        }

        componentWillMount() {
            if (!Auth.loggedIn() && !Auth.isAdmin()) {
                this.props.history.replace('/');
            } else {
                try {
                    const user = Auth.getUser();
                    this.setState({
                        user: user
                    });
                } catch (err) {
                    Auth.logout();
                    this.props.history.replace('/');
                }
            }
        }

        render() {
            if (this.state.user) {
                return(
                    <AuthComponent history={this.props.history} user={this.state.user}/>
                );
            } else {
                return null;
            }
        }
    }
}
